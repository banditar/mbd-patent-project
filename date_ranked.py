from pyspark.sql import SparkSession
from pyspark.sql.functions import col, year, month
from pyspark.sql import functions as func
import matplotlib.pyplot as plt

spark = SparkSession.builder.getOrCreate()

patents_path = "project/geoc_inv_person.csv"

def patents_by_month(df):
    '''
    Plot the number of patents by month by country
    '''
    df = df.orderBy(col("year").asc(), col("month").asc())

    plt.figure()
    
    years = [x for x in range(1980, 2014+1)]
    for year_tmp in years:
        print("Plotting year: ",year_tmp)
        selected_data = df.filter(col("year") == year_tmp)\
                .select(col("month"), col("count"))\
                .collect()
        plt.plot([row[0] for row in selected_data], [row[1] for row in selected_data])
    
    plt.xticks(range(1,12+1))
    plt.xlabel("Month")
    plt.ylabel("Patents")
    plt.title("Rank by month for each year")
    plt.savefig("images/distribution/plot_per_year.png")


def clean_data(df):
    '''
    Cleans the data by removing non-needed years
    '''
    return df.select(year("filing_date").alias("year"), month("filing_date").alias("month"))\
            .groupBy(col("year"), col("month")).count()\
            .filter(col("year") > 1900) # Remove null values

def total_patents_per_month(df):
    '''
    Plot the total patents (sum for each country) per month
    '''
    df = df.groupBy(col("month"))\
            .agg(func.sum("count").alias("total_count"))\
            .orderBy(col("month"))
    df.show()

    y = [val["total_count"] for val in df.collect()]
    x = [val["month"] for val in df.collect()]

    plt.figure()
    plt.plot(x,y, label = 'patents')
    plt.xticks(range(1,12+1))
    plt.xlabel("Month")
    plt.ylabel("Total patents")
    plt.title("Total patents per month")
    plt.savefig("images/distribution/total_patents_per_month.png")

if __name__ == "__main__":
    df = spark.read.option("header", "true").csv(patents_path)

    df = clean_data(df)
    patents_by_month(df)
    total_patents_per_month(df)
