# József-Hunor Jánosi
# s2832208
# time spark-submit --conf "spark.pyspark.python=/usr/bin/python3.6" --conf "spark.pyspark.driver.python=/usr/bin/python3.6" us_crime.py 2>/dev/null
# scp sID-HERE@ctit009.ewi.utwente.nl:mbd-patent-project/images/us_*.png /where/to/copy

from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, year
import matplotlib.pyplot as plt

spark = SparkSession.builder.appName('US_CRIMES').getOrCreate()

patents_path = '/user/s2881012/project/geoc_inv_person.csv'
crime_path = '/user/s2832208/project/ucr_crime_1975_2015.csv'

def get_crimes_sum():
    """
    Returns two lists.
    all_crimes: summed up every crime in a particular year (in the US)
    cat_crimes: summed up crimes by year in 5 different categories:
        homicide, rape, robbery, assault, violence.
    """
    print('Processing crimes data')

    # reading the data
    df = spark.read.option("header", "true").csv(crime_path)

    # only the relevant years
    df1 = df.filter(col('year') >= 1980).filter(col('year') <= 2014)

    # select and cast relevant columns into int
    df2 = df1.select(col('year'), col('homs_sum').cast('Int'), col('rape_sum').cast('Int'), col('rob_sum').cast('Int'), col('agg_ass_sum').cast('Int'), col('violent_crime').cast('Int'))

    # group by year
    df3 = df2.groupBy(col('year')).sum('homs_sum', 'rape_sum', 'rob_sum', 'agg_ass_sum', 'violent_crime')

    # alias them
    df4 = df3.select(col('year'), col('sum(homs_sum)').alias('homs'), col('sum(rape_sum)').alias('rape'), col('sum(rob_sum)').alias('rob'), col('sum(agg_ass_sum)').alias('ass'), col('sum(violent_crime)').alias('violent'))

    # sort by year
    df5 = df4.sort(col('year'))

    # sum them up into "all" crimes
    df6 = df5.select(col('year'), (col('violent')).alias('all'))

    # get the list of all crimes
    all_crimes = [c[1] for c in df6.collect()]

    # get crimes by category
    cat_crimes = [[c[1], c[2], c[3], c[4]] for c in df5.collect()]

    return all_crimes, cat_crimes, df6


def get_us_patents_sum():
    """
    Returns a list where each entry represents a year,
    and the values represent how many patents were issued
    in that year in the United States.
    """
    print('Processing patents data')

    # reading patents data
    df = spark.read.option("header", "true").csv(patents_path)

    # selecting only relevant columns
    df1 = df.select(year(col('filing_date')).alias('year'), col('ctry_code'))

    # only getting US patents
    df2 = df1.filter(col('ctry_code') == 'US').drop(col('ctry_code')).sort(col('year'))

    # grouping them by year
    df3 = df2.groupBy(col('year')).count()

    # getting the list of sums per year
    patents = [x[1] for x in df3.collect()]

    return patents, df3


def plot_all_crimes_patents(all_crimes, patents):
    """
    Plots a single figure.
    Represents all crimes and patents from 1980 and 2014, in the US.
    """
    print('Plotting all crimes')

    years = [y for y in range(1980, 2015)]

    plt.figure()
    plt.plot(years, patents, c='blue', label = 'Patents')
    plt.xlabel('Years')
    plt.ylabel('Patents', color='blue')
    plt.legend()

    # creating a second y axes, so both plots can be on the same figure
    plt.twinx()
    plt.plot(years, all_crimes, c='red', label = 'Crimes')
    plt.ylabel('Crimes', color='red')
    plt.legend()
    plt.title('Patents, and Crimes throughout the years in the US')
    plt.savefig('images/us_all_crimes')


def plot_cat_crimes_patents(cat_crimes, patents):
    """
    Plots a single figure.
    Represents crimes by categories and patents from 1980 and 2014, in the US.
    """

    print('Plotting crimes by category')

    years = [y for y in range(1980, 2015)]

    lines = [0]*5
    plt.figure()
    lines[0], = plt.plot(years, patents, c='blue', label = 'Patents')
    plt.xlabel('Years')
    plt.yticks([], [])
    plt.twinx()

    colors = ['red', 'purple', 'fuchsia', 'chocolate']
    labels = ['Homicide', 'Rape', 'Robbery', 'Assault']
    for i in range(4):
        crime = [item[i] for item in cat_crimes]
        lines[i+1], = plt.plot(years, crime, c=colors[i], label = labels[i])
        plt.axis('off')
        plt.twinx()

    plt.legend(handles=lines, loc=9)
    plt.yticks([], [])
    plt.title('Patents, and Crimes by category throughout the years in the US')
    plt.savefig('images/us_cat_crimes')


def calculate_correlation(crimes_df, patents_df):
    """
    Calculates and prints the correlation coefficient between the number
    of patents and the number of crimes in the US, between 1980 and 2014
    """
    print('Calculating coefficient')

    # join the two DFs
    merged_df = patents_df.join(crimes_df, on='year')
    # calculate Pearson Correlation Coefficient
    correlation = merged_df.corr('count', 'all')

    # it's -0.6336277990560236
    
    print(f'The number of patents correlates with the number of crimes in the United States between 1980 and 2014 with a coefficient of: {correlation}')

    return correlation


if __name__ == '__main__':
    all_crimes, cat_crimes, crimes_df = get_crimes_sum()

    patents, patents_df = get_us_patents_sum()

    plot_all_crimes_patents(all_crimes, patents)
    plot_cat_crimes_patents(cat_crimes, patents)

    calculate_correlation(crimes_df, patents_df)
