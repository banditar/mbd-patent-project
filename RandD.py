#pyspark --conf "spark.pyspark.python=/usr/bin/python3.6" --conf "spark.pyspark.driver.python=/usr/bin/python3.6"


from pyspark.sql import SparkSession
from pyspark.sql.window import Window
from pyspark.sql.functions import col, year, row_number, lit
from pyspark.sql.types import StructType, StructField, StringType, FloatType
import matplotlib.pyplot as plt
from operator import add
from functools import reduce
import numpy as np
spark = SparkSession.builder.getOrCreate()




def compute_correlation_coefficient(gdp_df, patent_df):
    '''
        Computes the correlation coefficient between the development of patents and the GERD of a country
    '''
    # Create an empty RDD
    empty_RDD = spark.sparkContext.emptyRDD()
 
    # Create schema
    columns= StructType([StructField('country',
                                  StringType(), True),
                    StructField('correlation',
                                FloatType(), True)])
 
    # Create empty RDD with expected schema
    correlations = spark.createDataFrame(data = empty_RDD,
                           schema = columns)
    for country_pat in countries_patent:
        print('country :', country_pat)
        if country_pat in countries_RD:
            country_GERD = country_pat

        else:

            if country_pat in aux.keys():

                country_GERD = aux[country_pat]
                print('We fixed country', country_GERD)
            else:
                print("ERROR WITH Country", country_pat)
                continue

        years = [x['TIME4'] for x in RD_df.filter(col('Country') == country_GERD).select('TIME4').distinct().orderBy(col('TIME4')).collect() ]
        years = [year for year in years if int(year)<2015] #We get a list of all the years available in both df (the ones in GERD, as long as they're more recent than 2015)

        patent_country_series = patent_df.filter(col("name_0").alias("country") == country_pat) \
            .groupBy(year("filing_date").alias("year")) \
                    .count() \
                            .orderBy(col("year").asc())

        country_data = RD_df.filter(col("Country") == country_GERD)

        if country_data.count() < 1:
            print("Error while finding country: " + country + " in GDP data")
            break


        RD_country_series = country_data.select(col('TIME4').alias('year'), col('Value').alias('GERD'))


        merged_df = patent_country_series.join(RD_country_series, on="year") \
            .withColumn("GERD", col("GERD").cast("Double")) \
            .withColumn("count", col("count").cast("Double"))

        pearson_correlation_coefficient = merged_df.corr("count", "GERD")
        newRow = spark.createDataFrame([(country_pat, pearson_correlation_coefficient)], columns)
        correlations = correlations.union(newRow)
        print("In " + country_pat + " the number of patents correlates with the growth of the GERD with a coefficient of \t" + str(pearson_correlation_coefficient))
        correlations.show(5)
    correlations.write.format('csv').save('file:///home/s2818337/PATENTS/GERD_correlation.csv')



def analyze_correlations(correlations_df):
	'''
	Analyzed the correlations DF by splitting it into 3
	'''
    high_correlations = correlations_df.filter(col('correlation') >= 0.8).orderBy(col('correlation').desc())
    high_correlations.show()
    high_correlations.count()
    medium_correlations = correlations_df.filter((col('correlation') < 0.8) & (col('correlation')>= 0.5)).orderBy(col('correlation').desc())
    medium_correlations.show()
    medium_correlations.count()    
    low_correlations = correlations_df.filter(col('correlation') < 0.1).orderBy(col('correlation').asc() )
    low_correlations.show()     
    low_correlations.count() 

def plot_countries_gerd_patents(list_countries_pat, patents, GERD):
    """
    Plots a single figure for each of the countries in list_countries_pat
    Represents all GERD and correlations from 1996 and 2014
    """
    plt.rcParams["figure.figsize"] = (12.8,9.6)

    for country_pat in list_countries_pat:
        if country_pat in countries_RD:
            country_GERD = country_pat

        else:

            if country_pat in aux.keys():

                country_GERD = aux[country_pat]
        years = [x['TIME4'] for x in RD_df.filter(col('Country') == country_GERD).select('TIME4').distinct().orderBy(col('TIME4')).collect() ]
        years = [year for year in years if int(year)<2015] #We get a list of all the years available in both df (the ones in GERD, as long as they're more recent than 2015)

        patent_country_series = patent_df.filter(col("name_0").alias("country") == country_pat) \
            .groupBy(year("filing_date").alias("year")) \
                    .count() \
                            .orderBy(col("year").asc())

        patent_country_series = patent_country_series.filter(col('year').isin(years))
        patents = [x[1] for x in patent_country_series.collect()]

        country_data = RD_df.filter((col("Country") == country_GERD) & (col('TIME4').isin(years))).orderBy(col("TIME4").asc())
        GERD = [x[6] for x in country_data.collect()]
        print('Plotting GERD-patents for :'+country_pat)


        plt.figure()
        line1=plt.plot(years, patents, c='blue', label = 'Patents')
        plt.xlabel('Years')
        plt.ylabel('Patents', color='blue')
        plt.legend(loc=0)

        # creating a second y axes, so both plots can be on the same figure
        plt.twinx()
        line2 =plt.plot(years, GERD, c='red', label = 'GERD')
        plt.ylabel('GERD', color='red')
        plt.legend(loc=0)
        plt.title('Patents, and GERD throughout the years in'+country_pat)
        plt.savefig('pics/'+country_pat+'.png')
        break



def define_countries_with_biggest_GERD(RD_df):
    '''
        Returns an ordered list with the countries considering their average GERD between 1996-2019
    '''
    RD_df = RD_df.withColumn("Value", RD_df.Value.cast('float'))
    countries_with_higher_GERD = RD_df.groupBy(col("Country")).avg("Value").orderBy(col("avg(Value)").desc())

if __name__ == "__main__":
    patent_path = "/user/s2881012/project/geoc_inv_person.csv"
    RD_path = "file:///home/s2818337/PATENTS/GERD_as_percent_GDP.csv"
    GERD_correlations_path = "file:///home/s2818337/PATENTS/GERD_correlation.csv"

    patent_df = spark.read.option("header", "true").csv(patent_path)

    RD_df = spark.read.option("header", "true").csv(RD_path)
    RD_df = RD_df.withColumn("Value", RD_df.Value.cast('float'))


    countries_RD = [x['Country'] for x in RD_df.select('Country').distinct().collect() ]
    countries_patent = [x['name_0'] for x in patent_df.select('name_0').distinct().collect() ]

    aux = {'Russia':'Russian Federation'
    ,'Czech Republic':'Czechia'#dictionary that contains the equivalnces of names between the 2 datasets
    ,'United Kingdom':'United Kingdom of Great Britain and Northern Ireland'
    ,'South Korea':'Republic of Korea'
    ,'United States':'United States of America'}

    compute_correlation_coefficient(RD_df, patent_df)
    corr = spark.read.option("header", "false").csv(GERD_correlations_path)
    corr = corr.select(col('_c0').alias('country'),col('_c1').alias('correlation'))
    analyze_correlations(corr)
    plot_countries = ['Spain','Turkey','Denmark','Brazil','South Korea', 'China', 'United States', 'Romania', 'Sweden', 'Japan', 'Monaco']
    plot_countries_gerd_patents(plot_countries, patent_df, RD_df)

