from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, year
from pyspark.sql.functions import asc
from pyspark.sql import functions as F
from pyspark.sql.functions import size,udf
from pyspark.sql.types import StringType
from pyspark.sql.types import FloatType
from functools import reduce
import matplotlib.pyplot as plt


spark = SparkSession.builder.appName('POPULATION').getOrCreate()


patents_path = "project/geoc_inv_person.csv"
population_path = "project/population_by_country.csv"

def ratio_patents_by_population(df):
    '''
    Compute the ratio of the patent by 100 thousand of inhabitants
    '''
    population_ratio = 100000.0

    # UDF used to divide the values of one column (array)
    divide = udf(lambda column: round((float(column[0]) / float(column[1]) * population_ratio),5), FloatType())

    df2 = spark.read.option("header","true").csv(population_path)
    years = [str(x) for x in range(1980, 2014+1)]
    years.insert(0, "Country Name")

    # Select all the needed years from the population dataset (since it starts way earlier)
    df_population = df2.select(years)

    # Get the df from the patents dataset
    df_patents = df.filter(year("filing_date").alias("year") > 1979)\
            .select(col("name_0").alias("Country Name"), year("filing_date").cast('int').alias("year"))\
            .groupBy(col("Country Name")).pivot("year").count()
            
    df_final = df_patents.union(df_population)\
            .groupBy("Country Name").agg(*[F.collect_list(c).alias(c) for c in df_patents.columns[1:]])\
            .where(reduce(lambda x, y: x & y,  (size(F.col(x)) > 1 for x in df_patents.columns[1:])))\
            .select(F.col("Country Name"), *(divide(F.col(c)).alias(c) for c in df_patents.columns[1:]))
    
    return df_final


def plot_per_country(df):
    '''
    Plot the average of the computed ratio of the countries 
    '''
    countries = list([x[0] for x in df.select("Country Name").collect()])
    years = [str(x) for x in range(1980, 2014+1)]
    years_int = [x for x in range(1980, 2014+1)] 
    for country in countries:
        plt.figure()
        country_ratio = df.filter(col("Country Name") == country) \
                .select(years).collect()
        
        ratios = [float(column) for column in country_ratio[0]]
        plt.plot(years_int, ratios, label = country)
        plt.xlabel("Years")
        plt.ylabel("Number of patents (each 100.000 people)")
        plt.title("Patents by year in "+country)
        plt.savefig("images/population/population_ratio_"+country)
        print("Saved "+country)


def plot_mean_countries(df):
    '''
    Given the dataframe, plots the mean of ratio of the patents by country
    '''
    countries = list([x[0] for x in df.select("Country Name").collect()])
    years = [str(x) for x in range(1980, 2014+1)]
    years_int = [x for x in range(1980, 2014+1)]

    ratio_per_year = []
    for year in years:
        df_per_year = df.select(year)
        lenght = df_per_year.count()
        ratio_per_year.append(df_per_year.groupBy().sum().collect()[0][0] / lenght)
        print("Year ",year," added")
    plt.figure()
    plt.plot(years_int, ratio_per_year)
    plt.xlabel("Years")
    plt.ylabel("Average ratio")
    plt.title("Average number of patents each 100.000 inhabitants")
    plt.savefig("images/population/average_by_inhabitants")
    print("Average number of patents saved")


if __name__ == '__main__':
    df = spark.read.option("header", "true").csv(patents_path)
    ranked_data = ratio_patents_by_population(df)
    ranked_data.show()

    # Writes on the hdfs
    ranked_data.write.csv(r"project/patents_by_population", mode = 'overwrite')
    plot_per_country(ranked_data)
    plot_mean_countries(ranked_data)


