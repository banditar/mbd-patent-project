# time spark-submit --conf "spark.pyspark.python=/usr/bin/python3.6" --conf "spark.pyspark.driver.python=/usr/bin/python3.6" human_dev_ind.py 2>/dev/null
# user	3m51.928s

from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, year
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

spark = SparkSession.builder.appName('HDI').getOrCreate()

patents_path = '/user/s2881012/project/geoc_inv_person.csv'
hdi_path = '/user/s2832208/project/human_development_index.csv'


def get_hdi():
    """
    Returns a list of the Human Development Index of the whole world
    between 1990, and 2014
    """
    # the dataset
    hdf = spark.read.option("header", "true").csv(hdi_path)
    # ordering by 2019 HDI ranking, where 'World' will be on top
    hdf_sort = hdf.sort(col('Ranking').cast('Int'))
    # dropping non-important column
    hdf2 = hdf_sort.drop(col('Ranking'))
    # retrieving the list with the HDI by years
    world_hdi = [float(x) for x in hdf2.take(1)[0][1:]]

    return world_hdi


def get_patents():
    """
    Returns a list of number of patents issued in the world
    between 1990 and 2014
    """
    # dataset
    pdf = spark.read.option("header", "true").csv(patents_path)
    # converting filing full date into year
    pdf2 = pdf.select(year(col('filing_date')).alias('year'))
    # filtering relevant dates
    pdf3 = pdf2.filter(col('year') >= 1990)
    # counting
    pdf4 = pdf3.groupBy(col('year')).count()
    # sorting
    pdf_sort = pdf4.sort(col('year'))
    # making into list
    patents = [x[1] for x in pdf_sort.collect()]

    return patents


def calculate_correlation(hdi, patents):
    """
    Calculates the pearson corr. coef. of two lists
    """

    (coeff,_) = pearsonr(patents, hdi)
    # 0.9055058328447603 --- world
    # 0.8862469992099208 --- India
    
    print('Correlation coefficient of patents and hdi in the world between 1990 and 2014:', coeff)
    
    return coeff


def plot_patents_hdi(patents, hdi):
    """
    Plots the two lists into one figure
    """
    # relevant years, as x
    years = [x for x in range(1990, 2015)]

    # for legend
    lines = [0]*2
    plt.figure()
    lines[0], = plt.plot(years, patents, c='blue', label = 'Patents')
    plt.xlabel('Years')
    plt.ylabel('Patents', color='blue')

    # doubling the y axis
    plt.twinx()
    lines[1], = plt.plot(years, hdi, c='red', label = 'World HDI')
    plt.ylabel('HDI', color='red')
    plt.legend(handles=lines)
    plt.title('Patents, and HDI throughout the years in the World')
    plt.savefig('images/hdi_world')


if __name__ == '__main__':
    hdi_world = get_hdi()

    patents = get_patents()

    plot_patents_hdi(patents, hdi_world)

    calculate_correlation(hdi_world, patents)
