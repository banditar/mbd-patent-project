# **Analysis of worldwide issued patents**

University of Twente\
Managing Big Data project -- Spark\
2021/2022\
Authors/students:
- Huber, Maximilian
- Jánosi, József-Hunor
- Santiago Garcia, Eric
- Verdecchia, Cristian


## Topics

### Patent data (`patents.py`): 
- How does the number of patents develop throughout the years between 1980 and 2014.
- Heatmap per year (because I didn’t find a library that can use that amount of memory that is needed for all)
- What are the ten countries that have the most entries?
- How does the number of patents develop between 1980 - 2014 in the top 10 countries?

### GDP (`gdp.py`):
- Are the countries with the highest GDP the same countries which apply for the most patents?
- How does the GDP correlate to the number of patents per year in a country?

### GERD (`RandD.py`):
Using the dataset created by the UNESCO Institute for Statistics, on the Gross Domestic Expenditure in R&D as a percentage of the GDP, we will answer the question:
- How does the expenditure in R&D correlate to the number of patents per year in a country?

### HDI (`human_dev_ind.py`):
Data used is provided by the United Nations Development Program, with the HDI of every country between 1990 and 2019.
- Is the number of patents issued correlated with the Human Development Index of the world?

### US Crime rate (`us_crime.py`):
Data used from FBI's Uniform Crime Reporting Program's 'Offenses Known and Clearances by Arrest' database,
which sums the committed crimes in 4 different categories: homicide, rape, aggrevated assault, and robbery.
- Does the number of committed crimes correlate with the number of patents issued in the United States from 1980 and 2014?

### Population (`population.py`):
The data used are from the World Bank, it contains entries for each country for each year from 1960 to 2020.
Population data are then compared to the patents data to find correlation between the population growth and the patents growth.

### Patents comparison by month (`date_ranked.py`):
Does the number of patents change along the year with respect to the months?
- Patents are summed for each year and plotted over the months
- All the patent for all the years are summed and plotted over the month


