from pyspark.sql import SparkSession
from pyspark.sql.functions import col, year
from pyspark.sql.types import StringType
from pyspark.sql.functions import asc
import matplotlib.pyplot as plt
import plotly.express as px
import os

spark = SparkSession.builder.getOrCreate()

patents_path = "project/geoc_inv_person.csv"

def plot_number_of_patents_per_year(df):
    '''
    Creates and saves a plot of the number of all patents over the years
    '''
    patents_per_year = df.groupBy(year("filing_date").alias("year")) \
        .count() \
        .orderBy(col("year").asc())

    y = [val["count"] for val in patents_per_year.collect()]
    x = [val["year"] for val in patents_per_year.collect()]

    plt.figure()
    plt.plot(x, y)

    plt.ylabel("number of patents")
    plt.xlabel("year")
    plt.title("Number of patents per year")
    plt.savefig("images/number_of_patents_per_year")


def determine_countries_with_most_patents(df):
    '''
    Returns a Dataframe with the 10 countries in which most patents over all years have been placed.
    '''
    countries_with_most_patents = df.groupBy(col("name_0").alias("country")).count().orderBy(col("count").desc())
    print("\n The 10 countries with most patent applications (1980 - 2014)")
    countries_with_most_patents.show(10)
    return countries_with_most_patents
    # +--------------+--------+
    # |       country|   count|
    # +--------------+--------+
    # |         Japan|12469106|
    # | United States| 6519506|
    # |   South Korea| 3926312|
    # |         China| 2349446|
    # |       Germany| 2267477|
    # |        France|  734644|
    # |United Kingdom|  371177|
    # |         Italy|  266718|
    # |        Canada|  260319|
    # |        Sweden|  162124|
    # +--------------+--------+


def plot_development_of_top_ten_countries_of_patents(df, countries_with_most_patents):
    '''
    Creates and saves plots of the development of issued patents over the years for the ten countries in which the most patents have been issued.
    '''
    for value in countries_with_most_patents.collect()[:10]:
        country = value.country
        timeseries_per_country = df.filter(col("name_0") == country) \
            .groupBy(year("filing_date").alias("year")) \
            .count()\
            .orderBy(col("year").asc())
        
        y = [val["count"] for val in timeseries_per_country.collect()]
        x = [val["year"] for val in timeseries_per_country.collect()]

        plt.figure()
        plt.plot(x, y)

        plt.ylabel("number of patents")
        plt.xlabel("year")
        plt.title("Number of patents per year in " + country)
        plt.savefig("patents_by_country/number_of_patents_" + country)


def create_world_heatmap(df):
    '''
    Creates and saves a heatmap of the world to illustrate where most patents have been issued.
    This is done for every year between 1980 and 2014, because of memory issues. 
    '''
    for year_int in range(1980, 2014+1):
        data = df.filter(year("filing_date").alias("year") == year_int) \
                .select( \
                    col("lat").alias("Latitude"), \
                    col("lng").alias("Longitude")) \
                .groupBy(col("Latitude"), col("Longitude")) \
                .count()
        fig = px.density_mapbox(data.toPandas(), lat='Latitude', lon='Longitude', z='count', radius=25, center=dict(lat=0, lon=5), zoom=2.5, width=2800, height=1600)
        fig.update_layout(mapbox_style="stamen-terrain", mapbox_center_lon=0, margin={"r":0,"t":0,"l":0,"b":0})
        fig.write_image("images/heatmaps/world_heatmap_" + str(year_int) + ".png")


def prepare_environment():
    try:
        os.mkdir("images")
        os.mkdir("images/heatmaps")
        os.mkdir("patents_by_country")
    except FileExistsError:
        # directory already exists
        pass



if __name__ == '__main__':
    df = spark.read.option("header", "true").csv(patents_path)
    prepare_environment()

    plot_number_of_patents_per_year(df)
    countries_with_most_patents = determine_countries_with_most_patents(df)
    plot_development_of_top_ten_countries_of_patents(df, countries_with_most_patents)
    create_world_heatmap(df)

