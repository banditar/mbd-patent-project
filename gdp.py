from pyspark.sql import SparkSession
from pyspark.sql.window import Window
from pyspark.sql.functions import col, year, row_number, lit
import matplotlib.pyplot as plt
from patents import determine_countries_with_most_patents
from operator import add
from functools import reduce

spark = SparkSession.builder.getOrCreate()

patent_path = "project/geoc_inv_person.csv"
gdp_path = "project/gdp.csv"

years = ['1980 [YR1980]', '1981 [YR1981]', '1982 [YR1982]', '1983 [YR1983]', '1984 [YR1984]', '1985 [YR1985]', '1986 [YR1986]', \
    '1987 [YR1987]', '1988 [YR1988]', '1989 [YR1989]', '1990 [YR1990]', '1991 [YR1991]', '1992 [YR1992]', '1993 [YR1993]', '1994 [YR1994]', \
        '1995 [YR1995]', '1996 [YR1996]', '1997 [YR1997]', '1998 [YR1998]', '1999 [YR1999]', '2000 [YR2000]', '2001 [YR2001]', '2002 [YR2002]', \
            '2003 [YR2003]', '2004 [YR2004]', '2005 [YR2005]', '2006 [YR2006]', '2007 [YR2007]', '2008 [YR2008]', '2009 [YR2009]', '2010 [YR2010]', \
                '2011 [YR2011]', '2012 [YR2012]', '2013 [YR2013]', '2014 [YR2014]']

def plot_gdp_of_biggest_countries(gdp_df):
    countries_with_biggest_gdp = define_countries_with_biggest_gdp(gdp_df)\
        .select(col("countries with biggest GDP").alias("country"))\
        .limit(10)

    for value in countries_with_biggest_gdp.collect():
        country = value.country
    
        timeseries_per_country = gdp_df.filter(col("Country Name") == country) \
                .select(years)
        for y in years:
            timeseries_per_country = timeseries_per_country.withColumn(y.split()[0], col(y).cast("Double"))\
                .drop(y)

        country_values = timeseries_per_country.collect()[0]

        y = [val for val in country_values]
        x = list(map(int, country_values.asDict().keys()))

        plt.figure()
        plt.plot(x, y)

        plt.ylabel("GDP")
        plt.xlabel("year")
        plt.title("GDP per year in " + country)
        plt.savefig("images/gdp/gdp_development_" + country)

    
def define_countries_with_biggest_gdp(gdp_df):
    '''
        Returns an ordered list with the countries considering their GDP between 1980-2014
    '''
    gdp_df = gdp_df.replace("..", "0").na.fill(value=0, subset=years) \
            .withColumn('total', reduce(add, [col(x).cast("Double") for x in years]))\
                .select(col("Country Name"), col("total"))
    #cleaning all ".." values with 0
    #then summing all years up
    gdp_sorting_window = Window.orderBy(col("total").cast("Double").alias("gdp").desc())
    return gdp_df.withColumn("id", row_number().over(gdp_sorting_window)) \
        .orderBy(col("id").asc()) \
            .select(col("id"), col("Country Name").alias("countries with biggest GDP"))

def print_countries_with_most_patents_and_biggest_gdp(gdp_df, patent_df):
    '''
        Prints the countries with most patents between 1980 - 2014 and the countries with the biggest GDP in 2014
    '''
    patent_sorting_window = Window.orderBy(lit(1))
    countries_with_most_patents = patent_df.groupBy(col("name_0").alias("country")) \
        .count() \
            .orderBy(col("count").desc()) \
                .withColumn("id", row_number().over(patent_sorting_window)) \
                .select(col("id"), col("country").alias("countries with most patents")) \
                    .limit(15)
    
    countries_with_biggest_gdp_in_2014 = define_countries_with_biggest_gdp(gdp_df)\
        .limit(15)

    countries_with_most_patents.join(countries_with_biggest_gdp_in_2014, on="id").show()
    # +---+---------------------------+--------------------------+
    # | id|countries with most patents|countries with biggest GDP|
    # +---+---------------------------+--------------------------+
    # |  1|                      Japan|             United States|
    # |  2|              United States|                     Japan|
    # |  3|                South Korea|                     China|
    # |  4|                      China|                   Germany|
    # |  5|                    Germany|            United Kingdom|
    # |  6|                     France|                    France|
    # |  7|             United Kingdom|                     Italy|
    # |  8|                      Italy|                    Brazil|
    # |  9|                     Canada|                     Spain|
    # | 10|                     Sweden|                     India|
    # | 11|                Switzerland|                 Australia|
    # | 12|                Netherlands|                    Mexico|
    # | 13|                    Finland|        Russian Federation|
    # | 14|                      India|               South Korea|
    # | 15|                     Israel|                    Canada|
    # +---+---------------------------+--------------------------+
    # 
    # 10/15 countries with the most patents are among the 15 countries with the biggest GDP (2014)

def compute_correlation_coefficient(gdp_df, patent_df):
    '''
        Computes the correlation coefficient between the development of patents and the GDP of a country
    '''

    years = list(gdp_df.drop("Series Name", "Series Code", "Country Code", "Country Name").columns)
    countries = list([x[0] for x in determine_countries_with_most_patents(patent_df).limit(10).select("country").collect()])

    for country in countries:
        patent_country_series = patent_df.filter(col("name_0").alias("country") == country) \
            .groupBy(year("filing_date").alias("year")) \
                    .count() \
                            .orderBy(col("year").asc())

        country_data = gdp_df.filter(col("Country Name") == country)

        if country_data.count() < 1:
            print("Error while finding country: " + country + " in GDP data")
            break

        gdp_country_series = spark.createDataFrame(\
                [(c.split()[0], country_data.select(c).collect()[0][0]) for c in years], ["year", "gdp"]) \
            .filter(col("year") < 2015)

        merged_df = patent_country_series.join(gdp_country_series, on="year") \
            .withColumn("gdp", col("gdp").cast("Double")) \
            .withColumn("count", col("count").cast("Double"))

        pearson_correlation_coefficient = merged_df.corr("count", "gdp")

        print("In " + country + " the number of patents correlates with the growth of the GDP with a coefficient of \t" + str(pearson_correlation_coefficient))
'''
    In Japan the number of patents correlates with the growth of the GDP with a coefficient of 	0.1446038940107474
    In United States the number of patents correlates with the growth of the GDP with a coefficient of 	0.9887937498559723
    In South Korea the number of patents correlates with the growth of the GDP with a coefficient of 	0.9389978743499415
    In China the number of patents correlates with the growth of the GDP with a coefficient of 	0.8235698825861101
    In Germany the number of patents correlates with the growth of the GDP with a coefficient of 	0.8145658373678591
    In France the number of patents correlates with the growth of the GDP with a coefficient of 	0.9767261142749888
    In United Kingdom the number of patents correlates with the growth of the GDP with a coefficient of 	0.9224590668879229
    In Italy the number of patents correlates with the growth of the GDP with a coefficient of 	0.8183128204663639
    In Canada the number of patents correlates with the growth of the GDP with a coefficient of 	0.953930187256695
    In Sweden the number of patents correlates with the growth of the GDP with a coefficient of 	0.8627713330595954
'''

if __name__ == '__main__':
    patent_df = spark.read.option("header", "true").csv(patent_path)
    gdp_df = spark.read.option("header", "true").csv(gdp_path)

    plot_gdp_of_biggest_countries(gdp_df)
    print_countries_with_most_patents_and_biggest_gdp(gdp_df, patent_df)
    compute_correlation_coefficient(gdp_df, patent_df)

